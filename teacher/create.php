<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
  
// get database connection
include_once '../config/database.php';
  
// instantiate teacher object
include_once '../objects/teacher.php';
  
$database = new Database();
$db = $database->getConnection();
  
$teacher = new Teacher($db);
  
// get posted data
$data = json_decode(file_get_contents("php://input"));
  
// make sure data is not empty
if(
    !empty($data->name) &&
    !empty($data->designation) &&
    !empty($data->dept) 
){
  
    // set teacher property values
    $teacher->name = $data->name;
    $teacher->designation = $data->designation;
    $teacher->dept = $data->dept;
  
    // create the teacher
    if($teacher->create()){
  
        // set response code - 201 created
        http_response_code(201);
  
        // tell the user
        echo json_encode(array("message" => "Teacher record is created."));
    }
  
    // if unable to create the teacher, tell the user
    else{
  
        // set response code - 503 service unavailable
        http_response_code(503);
  
        // tell the user
        echo json_encode(array("message" => "Unable to create teacher."));
    }
}
  
// tell the user data is incomplete
else{
  
    // set response code - 400 bad request
    http_response_code(400);
  
    // tell the user
    echo json_encode(array("message" => "Unable to insert teacher record. Data is incomplete."));
}
?>