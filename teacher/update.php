<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
  
// include database and object files
include_once '../config/database.php';
include_once '../objects/teacher.php';
  
// get database connection
$database = new Database();
$db = $database->getConnection();
  
// prepare teacher object
$teacher = new Teacher($db);
  
// get id of teacher to be edited
$data = json_decode(file_get_contents("php://input"));
  
// set ID property of teacher to be edited
$teacher->id = $data->id;
  
// set teacher property values
$teacher->name          = $data->name;
$teacher->designation   = $data->designation;
$teacher->dept          = $data->dept;
  
// update the teacher
if($teacher->update()){
  
    // set response code - 200 ok
    http_response_code(200);
  
    // tell the user
    echo json_encode(array("message" => "Teacher record is updated."));
}
  
// if unable to update the teacher, tell the user
else{
  
    // set response code - 503 service unavailable
    http_response_code(503);
  
    // tell the user
    echo json_encode(array("message" => "Unable to update teacher record."));
}
?>