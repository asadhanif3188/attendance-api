<?php
class Teacher{
  
    // database connection and table name
    private $conn;
    private $table_name = "teachers";
  
    // object properties
    public $id;
    public $name;
    public $designation;
    public $dept;
      
    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }

    // read teachers
    function read(){
    
        // select all query
        $query = "SELECT * FROM " . $this->table_name;
    
        // prepare query statement
        $stmt = $this->conn->prepare($query);
    
        // execute query
        $stmt->execute();
    
        return $stmt;
    } // end of function read

    // create teacher
    function create(){
    
        // query to insert record
        $query = "INSERT INTO
                    " . $this->table_name . "
                SET
                    name=:name, designation=:designation, dept=:dept";
    
        // prepare query
        $stmt = $this->conn->prepare($query);
    
        // sanitize
        $this->name         = htmlspecialchars(strip_tags($this->name));
        $this->designation  = htmlspecialchars(strip_tags($this->designation));
        $this->dept         = htmlspecialchars(strip_tags($this->dept));
    
        // bind values
        $stmt->bindParam(":name", $this->name);
        $stmt->bindParam(":designation", $this->designation);
        $stmt->bindParam(":dept", $this->dept);
    
        // execute query
        if($stmt->execute()){
            return true;
        }
    
        return false;
        
    }// end of function create

    // update the teacher
    function update(){
    
        // update query
        $query = "UPDATE
                    " . $this->table_name . "
                SET
                    name = :name,
                    designation = :designation,
                    dept = :dept
                WHERE
                    id = :id";
    
        // prepare query statement
        $stmt = $this->conn->prepare($query);
    
        // sanitize
        $this->name         = htmlspecialchars(strip_tags($this->name));
        $this->designation  = htmlspecialchars(strip_tags($this->designation));
        $this->dept         = htmlspecialchars(strip_tags($this->dept));
    
        // bind values
        $stmt->bindParam(":name", $this->name);
        $stmt->bindParam(":designation", $this->designation);
        $stmt->bindParam(":dept", $this->dept);
        $stmt->bindParam(':id', $this->id);
    
        // execute the query
        if($stmt->execute()){
            return true;
        }
    
        return false;
    } // end of function update

    // delete the teacher
    function delete(){
    
        // delete query
        $query = "DELETE FROM " . $this->table_name . " WHERE id = ?";
    
        // prepare query
        $stmt = $this->conn->prepare($query);
    
        // sanitize
        $this->id=htmlspecialchars(strip_tags($this->id));
    
        // bind id of record to delete
        $stmt->bindParam(1, $this->id);
    
        // execute query
        if($stmt->execute()){
            return true;
        }
    
        return false;
    } // end of function delete

} // end of class Teacher
?>